<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Михуил
 * Date: 08.09.14
 * Time: 5:04
 * To change this template use File | Settings | File Templates.
 */
?><!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Form for add goods</title>
	<style type="text/css">
		label{display: block;}
	</style>
</head>
<body>

<?php echo form_open('transfer');?>

<label for="from">Счет, откуда перевести</label>

	<? echo form_dropdown('from', $accounts); ?>


<label for="to">Счет, куда перевести</label>
<? echo form_dropdown('to', $accounts); ?>
<label for="amount">Сумма</label>
<input type="text" name="amount" id="amount" value="<?php echo set_value('amount') ?>">
<input name="transfer" type="submit" value="Создать">

<?php echo form_close();?>

<?php echo validation_errors(); ?>

</body>
</html>