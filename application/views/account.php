<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Михуил
 * Date: 08.09.14
 * Time: 6:24
 * To change this template use File | Settings | File Templates.
 */?><!DOCTYPE HTML>
<html lang="en-US" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
	<meta charset="UTF-8">
	<title>Form for add goods</title>
	<style type="text/css">
		label {
			display: block;
		}

		td {
			padding: 10px;
			text-align: center;
			border: solid 1px;
		}
	</style>
</head>
<body>
<? if (count($transactions) == 0) {
	echo 'Транзакции по этому счету не найдены';
} else {
	?>
<table style="">
	<tr>
		<th>Дата</th>
		<th>Номер счета отправителя<br> или получателя</th>
		<th>Приход</th>
		<th>Расход</th>
		<th>Остаток</th>
	</tr>
	<? foreach ($transactions as $transaction) {
	?>
	<tr>
		<td><?=$transaction['date']?></td>
		<td><?=$transaction['serial_receiver']?></td>
		<td><?=$transaction['prihod']?></td>
		<td><?=$transaction['rashod']?></td>
		<td><?=$transaction['ostatok']?></td>
	</tr>
	<? } ?>
</table>
	<? }?>
</body>
</html>