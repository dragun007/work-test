<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Михуил
 * Date: 08.09.14
 * Time: 6:07
 * To change this template use File | Settings | File Templates.
 */?><!DOCTYPE HTML>
<html lang="en-US" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
	<meta charset="UTF-8">
	<title>Form for add goods</title>
	<style type="text/css">
		label {
			display: block;
		}

		td {
			padding: 10px;
			text-align: center;
			border: solid 1px;
		}

	</style>
</head>
<body>
<?if (count($accounts) == 0) {
	echo 'Аккаунты не найдены';
} else {
	?>
<table>
	<tr>
		<th>Имя клиента</th>
		<th>Номер счета</th>
		<th>Баланс</th>
	</tr>
	<? foreach ($accounts as $account) {
	?>
	<tr>
		<td><?=$account['client']?></td>
		<td><a href="/accounts/<?=$account['serial']?>"><?=$account['serial']?></a></td>
		<td><?=$account['balance']?></td>
	</tr>
	<? } ?>
</table>
	<? }?>
</body>
</html>