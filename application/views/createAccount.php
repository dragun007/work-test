<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Михуил
 * Date: 08.09.14
 * Time: 3:36

 */?><!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Form for add goods</title>
    <style type="text/css">
	label{display: block;}
    </style>
</head>
<body>

<?php echo form_open('createAccount');?>

<label for="client">Имя клиента</label>
<input type="text" name="client" id="client" value="<?php echo set_value('client') ?>">
<label for="serial">Номер</label>
<input type="text" name="serial" id="serial" value="<?php echo set_value('serial') ?>">
<label for="balance">Баланс</label>
<input type="text" name="balance" id="balance" value="<?php echo set_value('balance') ?>">

<input name="create" type="submit" value="Создать">

<?php echo form_close();?>

<?php echo validation_errors(); ?>

</body>
</html>