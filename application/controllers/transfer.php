<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Михуил
 * Date: 08.09.14
 * Time: 4:35
 * To change this template use File | Settings | File Templates.
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transfer extends CI_Controller {

	function __construct(){
		parent::__construct();
		// load helpers, libraries, models
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('Transaction');
	}

	public function index()
	{

		if ($this->Transaction->add()) {
			echo 'Data add successful!';
		}
		else {
			$query = $this->db->get('accounts');
			$accounts=$query->result_array();
			foreach($accounts as $account)
			{
				$out[$account['serial']]=$account['client'].': '.$account['serial'];
			}

			$data['accounts'] = $out;

			$this->load->view('transfer',$data);
		}



	}
}