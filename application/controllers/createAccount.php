<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Михуил
 * Date: 08.09.14
 * Time: 4:19
 * To change this template use File | Settings | File Templates.
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CreateAccount extends CI_Controller {

	function __construct(){
		parent::__construct();
		// load helpers, libraries, models
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model('Account');
	}

	public function index()
	{

		if ($this->Account->add()) {
			echo 'Data add successful!';
		}
		else {
			$this->load->view('createAccount');
		}

	}
}