<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Михуил
 * Date: 08.09.14
 * Time: 4:34
 * To change this template use File | Settings | File Templates.
 */
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Accounts extends CI_Controller {

	function __construct(){
		parent::__construct();
		// load helpers, libraries, models

		$this->load->model('Transaction');
	}
	function _remap($method)
	{
		if (method_exists($this, $method))
		{
			$this->$method();
		}
		else {
			$this->index($method);
		}
	}
	public function index($serial='')
	{

		if($serial=='')
		{
			$query = $this->db->get('accounts');
			$data['accounts'] = $query->result_array();
			$this->load->view('accounts',$data);
		}
		elseif(is_numeric($serial))
		{

			$data['transactions'] = $this->Transaction->getBySerial($serial);
			$this->load->view('account',$data);
		}
		else
		{
			echo 'Счет должен быть числом!';
		}
	}
}