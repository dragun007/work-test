<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Михуил
 * Date: 08.09.14
 * Time: 3:53
 * To change this template use File | Settings | File Templates.
 */
class Account extends CI_Model {

	CONST TYPE_IN = 1;
	CONST TYPE_OUT = 2;
	var $table = 'accounts';
	var $key_id = 'id';
	var $add_rules = array(
		array(
			'field' => 'client',
			'label' => 'Имя клиента',
			'rules' => 'required|min_length[2]|max_length[20]'
		),
		array(
			'field' => 'balance',
			'label' => 'Баланс',
			'rules' => 'required|numeric|max_length[10]'
		),
		array(
			'field' => 'serial',
			'label' => 'Номер',
			'rules' => 'required|numeric|max_length[10]|is_unique[accounts.serial]'
		),
	);


	function __construct(){
		parent::__construct();
	}



	function add(){

		$this->form_validation->set_rules($this->add_rules);

		if($this->form_validation->run()){

			$data = array();

			foreach ($this->add_rules as $one) {

				$f = $one['field'];

				$data[$f] = $this->input->post($f);
			}

			$this->db->insert($this->table, $data);
			//при создании акка создаем транзакицию на его сумму
			$this->load->model('Transaction');
			$this->db->insert('transactions', array('serial_account'=>$data['serial'],
				'serial_receiver'=>0, 'amount'=> $data['balance'], 'type'=>self::TYPE_IN));

			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
}