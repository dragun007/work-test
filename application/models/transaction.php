<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Михуил
 * Date: 08.09.14
 * Time: 3:53
 * To change this template use File | Settings | File Templates.
 */
class Transaction extends CI_Model {

	CONST TYPE_IN = 1;
	CONST TYPE_OUT = 2;
	CONST SYSTEM_ACC = 0;
	CONST COMMISSON = "0.99";
	var $table = 'transactions';
	var $key_id = 'id';
	var $add_rules = array(
		array(
			'field' => 'to',
			'label' => 'Счет, куда перевести',
			'rules' => 'required|integer|callback_serial_check'
		),
		array(
			'field' => 'from',
			'label' => 'Счет, откуда перевести',
			'rules' => 'required|integer|callback_serial_check'
		),
		array(
			'field' => 'amount',
			'label' => 'Cумма',
			'rules' => 'required|numeric'
		),
	);


	function __construct(){
		parent::__construct();
	}

	/**
	 * add data
	 */
	public function serial_check($str)
	{
		$this->db->select('*');
		$this->db->from('accounts');
		$this->db->where('serial', $str);

		if ($this->db->count_all_results()==0)
		{
			$this->form_validation->set_message('username_serial_check', '%s Клиента с таким номером не существует');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	function getBySerial($serial,$balance=false)
	{
		$this->db->from('transactions');
		$this->db->where('serial_account',$serial);
		$this->db->order_by("date", "asc");
		$query = $this->db->get();
		$result = $query->result_array();
		$ostatok=0;
		$data=array();
		foreach($result as $row)
		{
			$row['prihod']='';
			$row['rashod']='';
			if($row['type']==self::TYPE_IN)//прибавляем
			{
				$row['prihod']=$row['amount'];
				$ostatok=  bcadd ( $row['amount'] , $ostatok ,2 );
			}
			else//вычитаем
			{
				$row['rashod']=$row['amount'];
				$ostatok =  bcsub (  $ostatok ,  $row['amount'] ,2 );
			}
			$row['ostatok']=$ostatok;
			$data[]=$row;
		}
		if($balance)//последний остаток и есть наш баланс
			return $ostatok;
		else
			return  $data;
	}



	function add(){


		$this->form_validation->set_rules($this->add_rules);

		if($this->form_validation->run()){

			$data = array();

			foreach ($this->add_rules as $one) {

				$f = $one['field'];

				$data[$f] = $this->input->post($f);
			}



			//списываем
			$this->db->insert($this->table, array('serial_account'=>$data['from'],
				'serial_receiver'=>$data['to'], 'amount'=> $data['amount'], 'type'=>self::TYPE_OUT));

			//Считаем комписию через bcmul
			$com_Mod=bcmul('0.01', self::COMMISSON, 5);
			$commission=bcmul($data['amount'], $com_Mod, 2);

			//снимаем
			$this->db->insert($this->table, array('serial_account'=>$data['from'],
				'serial_receiver'=>self::SYSTEM_ACC, 'amount'=> $commission, 'type'=>self::TYPE_OUT));
			//приписываем коммисию
			$this->db->insert($this->table, array('serial_account'=>self::SYSTEM_ACC,
				'serial_receiver'=>$data['from'], 'amount'=> $commission, 'type'=>self::TYPE_IN));

			//приписываем
			$this->db->insert($this->table, array('serial_account'=>$data['to'],
				'serial_receiver'=>$data['from'], 'amount'=> $data['amount'], 'type'=>self::TYPE_IN));

			//обновляем баланс задетых акков(надежнее полностью пересчитать)
			$this->updateAccount($data['from']);
			$this->updateAccount($data['to']);


			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	public function updateAccount($serial)
	{
		$balance=$this->getBySerial($serial,$balance=true);
		$this->db->where('serial', $serial);
		$this->db->update('accounts', array('balance'=>$balance));
	}
}